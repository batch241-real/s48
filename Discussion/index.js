// Mock Database
let posts = [];

// Post ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

// ADD Post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// prevents the default behavior of an event
	// prevent the page from reloading (default of submit)
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	count++;
	console.log(posts);
	alert('Post successfully added!');
	showPosts();
});

// RETRIEVE post
const showPosts = () => {
	// variable that will contain all the posts
	let postEntries = '';
	// loop through array items
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	// console.log(postEntries);
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// EDIT post
const editPost = (id) => {

	// function first uses the querySelector() method to get the element with the id '#post-title-${id}' and '#post-body-${id}' and assigns its innerHTML property to the title variable with the same body

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};


// UPDATE post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Post successfully updated")
		}
	}
});


// ACTIVTY DELETE post
const deletePost = (id) => {
	posts.splice(-id, 1);
	showPosts(posts);
	console.log(id)
	console.log(posts);
	alert("Post deleted successfully!")
}